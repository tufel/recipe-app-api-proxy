# recipe-app-api-proxy

NGINX app for our recipe app API

## USAGE

### Environment variables

 * `LISTEN_PORT` - PORT TO LISTEN ON (DEFAULT: `8000`)
 * `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
 * `APP_port` - Port of the app to forward request to (default: `9000`) 